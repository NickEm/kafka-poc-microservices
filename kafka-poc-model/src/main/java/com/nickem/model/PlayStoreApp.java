package com.nickem.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.time.LocalDate;

import lombok.Data;

@Data
public class PlayStoreApp {

    private String app;
    private String category;
    private Double rating;
    private Integer reviews;
    private String size;
    private String installs;
    private String type;
    private double price;
    private String contentRating;
    private String genres;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate lastUpdated;
    private String currentVer;
    private String androidVer;

}
