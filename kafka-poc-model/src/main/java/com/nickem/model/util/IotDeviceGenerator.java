package com.nickem.model.util;

import com.nickem.model.IotDevice;
import com.nickem.model.IotDeviceActivity;
import com.nickem.model.IotDeviceStrategy;

import org.apache.commons.lang3.tuple.Pair;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import lombok.experimental.UtilityClass;

@UtilityClass
public class IotDeviceGenerator {

    private static final String DEVICE_SERIAL_PATTERN = "account-serial:%s:device-serial:%s";
    private static final String ACCOUNT_SERIAL_PATTERN = "account-serial:%s";
    private static final int ACCOUNTS_NUMBER_COUNT = 3;

    public static Collection<IotDevice> generateIotDevice(final int devicesCount) {
        final List<IotDevice> devices = new ArrayList<>();
        for (int i = 0; i < devicesCount; i++) {
            final int accountNumber = new Random().nextInt(ACCOUNTS_NUMBER_COUNT);
            final String accountSerial = String.format(ACCOUNT_SERIAL_PATTERN, accountNumber);
            devices.add(new IotDevice(UUID.randomUUID(), String.format(DEVICE_SERIAL_PATTERN, accountNumber, i), accountSerial));
        }

        return devices;
    }

    public static Pair<IotDevice, List<IotDeviceActivity>> generateIotDeviceActivity(final IotDevice device,
                                                                                     final IotDeviceStrategy strategy,
                                                                                     final int activitiesCount,
                                                                                     final long activityRate) {
        final List<IotDeviceActivity> iotDeviceActivities = new ArrayList<>();

        IotDeviceActivity lastActivity = IotDeviceActivity.builder()
                         .id(UUID.randomUUID())
                         .deviceSerialNumber(device.getDeviceSerialNumber())
                         .downloadBytes(0L)
                         .uploadBytes(0L)
                         .sessionStartTime(Instant.now().toEpochMilli())
                         .build();

        for (int i = 0; i < activitiesCount; i++) {
            switch (strategy) {
                case INCREMENTAL:
                    lastActivity = IotDeviceActivity.builder()
                                                    .id(UUID.randomUUID())
                                                    .deviceSerialNumber(lastActivity.getDeviceSerialNumber())
                                                    .downloadBytes(lastActivity.getDownloadBytes() + activityRate)
                                                    .uploadBytes(lastActivity.getUploadBytes() + activityRate)
                                                    .sessionStartTime(lastActivity.getSessionStartTime())
                                                    .build();
                    iotDeviceActivities.add(lastActivity);
                    break;
                case STATIC:
                    final IotDeviceActivity activity = IotDeviceActivity.builder()
                                                    .id(UUID.randomUUID())
                                                    .deviceSerialNumber(lastActivity.getDeviceSerialNumber())
                                                    .downloadBytes(activityRate)
                                                    .uploadBytes(activityRate)
                                                    .sessionStartTime(lastActivity.getSessionStartTime())
                                                    .build();
                    iotDeviceActivities.add(activity);
                    break;
            }
        }


        return Pair.of(device, iotDeviceActivities);
    }

}
