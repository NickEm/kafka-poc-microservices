package com.nickem.model;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IotDevice {

    private UUID id;
    private String deviceSerialNumber;
    private String accountSerialNumber;
}
