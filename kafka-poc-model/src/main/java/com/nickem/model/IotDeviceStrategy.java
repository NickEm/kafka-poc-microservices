package com.nickem.model;

public enum IotDeviceStrategy {
    STATIC,
    INCREMENTAL
}
