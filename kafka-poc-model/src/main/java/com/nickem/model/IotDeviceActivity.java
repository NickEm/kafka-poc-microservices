package com.nickem.model;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IotDeviceActivity {

    private UUID id;
    private String deviceSerialNumber;
    private Long downloadBytes;
    private Long uploadBytes;
    private Long sessionStartTime;
    private String accountSerialNumber;

}

