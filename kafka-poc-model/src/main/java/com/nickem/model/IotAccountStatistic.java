package com.nickem.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IotAccountStatistic {

    private String accountSerialNumber;
    private long downloadBytes;
    private long uploadBytes;
    private long startWindow;
    private long endWindow;
}
