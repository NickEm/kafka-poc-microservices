## Kafka Producer Consumer Model
### Description
This application execution flow:
* Producer side
	* reads data from `csv`. It's configured in [property file](kafka-poc-producer/src/main/resources/application.yaml) by 
	property`play-store-app-csv-file` 
	* using Spring batch job writes the data to Kafka topic `kafka-topics.play-store-app.name` (by property)
	
* Consumer side
	* reads data from the topic `kafka-topics.play-store-app.name` (by property)
	* writes it to Postgres DB with defined configuration in [docker compose config](docker-compose-poc.yml):

### Prerequisites
* You should have `docker` and `docker-compose` installed

### Installation

1. To build newest targets of all modules run:
 
		./gradlew clean build
2. To run application execute steps:

* Create new docker network that is used here for better localization:
 	
 		docker network create kafka-local-network

* Start kafka+zookeeper duet in the folder root:
	
		docker-compose -f docker-compose-kafka.yml up

* Start application in other terminal window:		
		
		docker-compose -f docker-compose-poc.yml up --build
		
* Execute in the another terminal window:		
		
		curl -X POST 'http://localhost:8081/start/play-store-app'
		
* As a result you should be able to connect to target DB and take a look on tables with records:
	
		SELECT * FROM kafka_consumer.kafka_processing kp ORDER BY kp.record_id ASC;
