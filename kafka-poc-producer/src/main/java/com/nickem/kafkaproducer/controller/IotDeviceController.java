package com.nickem.kafkaproducer.controller;

import com.nickem.kafkaproducer.service.IotDeviceProducer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/start")
public class IotDeviceController {

    private final IotDeviceProducer iotDeviceProducer;

    public IotDeviceController(final IotDeviceProducer iotDeviceProducer) {
        this.iotDeviceProducer = iotDeviceProducer;
    }

    @PostMapping("/iot-activities")
    public ResponseEntity startIotActivityGeneration() {
        iotDeviceProducer.produceActivities();
        return ResponseEntity.ok("Started IoT activities");
    }

    @PostMapping("/iot-devices")
    public ResponseEntity startIotDeviceGeneration() {
        iotDeviceProducer.produceDevices();
        return ResponseEntity.ok("Started IoT devices producing");
    }
}
