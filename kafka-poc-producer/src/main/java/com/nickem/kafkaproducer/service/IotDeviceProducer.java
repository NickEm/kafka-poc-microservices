package com.nickem.kafkaproducer.service;

import com.nickem.model.IotDevice;
import com.nickem.model.IotDeviceActivity;
import com.nickem.model.IotDeviceStrategy;
import com.nickem.model.util.IotDeviceGenerator;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class IotDeviceProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(IotDeviceProducer.class);

    private static final Integer ACTIVITY_PUBLISHING_PAUSE = 1000;
    private static final int WORKING_DEVICE_COUNT = 3;
    private static final int ACTIVITIES_COUNT = 10;

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String iotDeviceTopic;
    private final String iotDeviceActivityTopic;
    private final List<IotDevice> iotDevices;

    public IotDeviceProducer(final KafkaTemplate<String, String> kafkaTemplate,
                             @Value("${kafka-topics.iot-device.name}") final String iotDeviceTopic,
                             @Value("${kafka-topics.iot-device-activity.name}") final String iotDeviceActivityTopic) {
        this.kafkaTemplate = kafkaTemplate;
        this.iotDeviceTopic = iotDeviceTopic;
        this.iotDeviceActivityTopic = iotDeviceActivityTopic;
        this.iotDevices = generateIotDevices();
    }

    private List<IotDevice> generateIotDevices() {
        return Arrays.asList(
                new IotDevice(UUID.fromString("f1079ca8-89ba-4f5d-ae89-1a1e4d2a1d81"), "account-serial:0:device-serial:0", "account-serial:0"),
                new IotDevice(UUID.fromString("9a41c3c7-498d-4205-ae86-ae7fd003726f"), "account-serial:2:device-serial:1", "account-serial:2"),
                new IotDevice(UUID.fromString("b7ccaa69-01ee-4e8e-8cf0-120226339b7e"), "account-serial:2:device-serial:2", "account-serial:2"),
                new IotDevice(UUID.fromString("c2314502-2648-4aa0-9e46-dd0c0d1b65a8"), "account-serial:2:device-serial:3", "account-serial:2"),
                new IotDevice(UUID.fromString("83217705-51e3-494e-b4c4-a9cb18d38755"), "account-serial:2:device-serial:4", "account-serial:2"),
                new IotDevice(UUID.fromString("80a4fc73-62cc-467b-8cc1-ac5c0e44a808"), "account-serial:0:device-serial:5", "account-serial:0"),
                new IotDevice(UUID.fromString("1ac34a11-263d-4028-b172-ac6c6845431a"), "account-serial:2:device-serial:6", "account-serial:2"),
                new IotDevice(UUID.fromString("6ea0b24a-790d-4aed-8cf8-f1181ad41bdc"), "account-serial:1:device-serial:7", "account-serial:1"),
                new IotDevice(UUID.fromString("6f72e7b7-4d9c-4b7a-89cb-6b3582c8730a"), "account-serial:2:device-serial:8", "account-serial:2"),
                new IotDevice(UUID.fromString("2787ce92-e730-4e06-bb16-208db37615ed"), "account-serial:2:device-serial:9", "account-serial:2")
        );
    }

    @Async
    public void produceActivities() {
        final List<Pair<IotDevice, Iterator<IotDeviceActivity>>> deviceToActivityIterator = new ArrayList<>();

        for (int deviceIndex = 0; deviceIndex < WORKING_DEVICE_COUNT; deviceIndex++) {
            final Pair<IotDevice, List<IotDeviceActivity>> deviceToActivities = IotDeviceGenerator
                    .generateIotDeviceActivity(iotDevices.get(deviceIndex), IotDeviceStrategy.STATIC,
                                               new Random().nextInt(ACTIVITIES_COUNT), deviceIndex + 1);
            LOGGER.info("Device {} generated {} activities", deviceToActivities.getLeft().getDeviceSerialNumber(),
                     deviceToActivities.getRight().size());
            deviceToActivityIterator.add(Pair.of(deviceToActivities.getLeft(), deviceToActivities.getRight().iterator()));
        }

        for (int activityIndex = 0; activityIndex < ACTIVITIES_COUNT; activityIndex++) {
            for (final Pair<IotDevice, Iterator<IotDeviceActivity>> iotDeviceIteratorPair : deviceToActivityIterator) {
                if (iotDeviceIteratorPair.getRight().hasNext()) {
                    sendDeviceActivity(iotDeviceIteratorPair.getRight().next(), iotDeviceActivityTopic);
                }
            }
            try {
                Thread.sleep(ACTIVITY_PUBLISHING_PAUSE);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Async
    public void produceDevices() {
        for (final IotDevice device : iotDevices) {
            final Message<IotDevice> message = MessageBuilder
                    .withPayload(device)
                    .setHeader(KafkaHeaders.TOPIC, iotDeviceTopic)
                    .setHeader(KafkaHeaders.MESSAGE_KEY, device.getDeviceSerialNumber())
                    .build();
            LOGGER.info("Sending message to: [{}] with payload: \n {}", iotDeviceTopic, device);
            kafkaTemplate.send(message);
        }
    }

    private void sendDeviceActivity(final IotDeviceActivity iotDeviceActivity, final String topic) {
        final Message<IotDeviceActivity> message = MessageBuilder
                .withPayload(iotDeviceActivity)
                .setHeader(KafkaHeaders.TOPIC, topic)
                .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
                .build();
        LOGGER.info("Sending message to: [{}] with payload: \n {}", iotDeviceActivityTopic, iotDeviceActivity);
        kafkaTemplate.send(message);
    }
}
