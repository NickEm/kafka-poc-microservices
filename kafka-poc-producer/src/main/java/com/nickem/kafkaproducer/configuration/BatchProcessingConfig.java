package com.nickem.kafkaproducer.configuration;

import com.nickem.model.PlayStoreApp;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class BatchProcessingConfig {

    private static final double DEFAULT_DOUBLE = 1.2345d;

    @Bean
    public ItemReader<PlayStoreApp> googlePlayAppItemReader(
            @Value("${play-store-app-csv-file}") final String csvFilePath) throws UnexpectedInputException, ParseException {
        return new FlatFileItemReaderBuilder<PlayStoreApp>()
                .name("google-play-app-csv-reader")
                .resource(new ClassPathResource(csvFilePath))
                .linesToSkip(1)
                .fieldSetMapper(fieldSet -> {
                    PlayStoreApp app = new PlayStoreApp();
                    app.setApp(fieldSet.readString("App"));
                    app.setCategory(fieldSet.readString("Category"));
                    app.setRating(parseDouble(fieldSet, "Rating"));
                    app.setReviews(fieldSet.readInt("Reviews"));
                    app.setSize(fieldSet.readString("Size"));
                    app.setInstalls(fieldSet.readString("Installs"));
                    app.setType(fieldSet.readString("Type"));
                    app.setPrice(parseDouble(fieldSet, "Price"));
                    app.setContentRating(fieldSet.readString("Content Rating"));
                    app.setGenres(fieldSet.readString("Genres"));
                    app.setLastUpdated(LocalDate.parse(fieldSet.readString("Last Updated"),
                                                       DateTimeFormatter.ofPattern("MMMM d, yyyy")));
                    app.setCurrentVer(fieldSet.readString("Current Ver"));
                    app.setAndroidVer(fieldSet.readString("Android Ver"));

                    return app;
                })
                .delimited().delimiter(",").names( new String[]
                        {
                                "App",
                                "Category",
                                "Rating",
                                "Reviews",
                                "Size",
                                "Installs",
                                "Type",
                                "Price",
                                "Content Rating",
                                "Genres",
                                "Last Updated",
                                "Current Ver",
                                "Android Ver"

                        }
                )
                .build();
    }

    private double parseDouble(final FieldSet fieldSet, final String fieldName) {
        double result;
        try {
            String resultStr = fieldSet.readString(fieldName).replace("$", "");
            result = Double.valueOf(resultStr);
        } catch (NumberFormatException e) {
            result = DEFAULT_DOUBLE;
        }
        return result;
    }
}
