Kafka Postgres Consumer

Installation

1. To build new docker image execute:
 
		./gradlew :kafka-poc-producer:clean :kafka-poc-producer:build && cd kafka-poc-producer && docker build -t kafka-producer . && cd ../
		
2. To run it execute:
	
		docker run --rm --name kafka-producer -p 8081:8081 --network kafka-local-network -e SERVER_PORT=8081 kafka-producer