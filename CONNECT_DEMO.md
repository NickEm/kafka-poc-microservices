## Kafka Sink Connector Demo

This application execution flow:
* Producer side
	* reads data from `csv`. It's configured in [property file](kafka-poc-producer/src/main/resources/application.yaml) by 
	property`play-store-app-csv-file` 
	* using Spring batch job writes the data to Kafka topic `kafka-topics.play-store-app.name` (by property)
	
* Sink Connect side
	* reads data from the topic `kafka-topics.play-store-app.name` (by property)
	* post data to public REST endpoint (RPC) that is defined in [connector properties file](kafka-connect-poc-rpc/connect-sink-rps.json)

* Consumer side. That plays role of RPC service holder at the same time.
	* has opened REST endpoint for accepting POST requests and stores the data in underlying PostgresDB

### Prerequisites
* You should have `docker` and `docker-compose` installed

#### Installation

1. Build all projects:

		./gradlew clean build

2. Build uber jar for RPC (Remote Procedure Call) sink connector:
	
		./gradlew :kafka-connect-poc-rpc:clean :kafka-connect-poc-rpc:build :kafka-connect-poc-rpc:shadowJar
		
3. Move it to suitable connect folder:

		./kafka-connect-poc-rpc/prepare-connect-rpc.sh
		
4. Start all the needed services using `docker-compose`:

		 docker-compose -f docker-compose-all-for-connect.yml up --build
		 
5. Execute next `cURL` for starting our RPC connector :

		 curl -d @./kafka-connect-poc-rpc/connect-sink-rps.json -H "Content-Type: application/json" -X POST http://localhost:8083/connectors
		 
6. Execute next `cURL` for starting producer side which triggers whole flow:

		 curl -X POST http://localhost:8081/start/play-store-app

7. You can connect to defined destination Postgres DB and see the new records like:

		 SELECT * FROM kafka_consumer.play_store_app;