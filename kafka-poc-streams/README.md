Kafka POC streams	
	
	./gradlew :kafka-poc-streams:dependencies
	
	./gradlew :kafka-poc-streams:clean :kafka-poc-streams:build && cd kafka-poc-streams && docker build -t kafka-poc-streams . && cd ../
	
	docker run --rm --name kafka-poc-streams --network kafka-local-network kafka-poc-streams
