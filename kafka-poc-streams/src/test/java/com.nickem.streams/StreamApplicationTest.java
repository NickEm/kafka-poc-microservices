package com.nickem.streams;

import com.nickem.model.IotAccountStatistic;
import com.nickem.model.IotDevice;
import com.nickem.model.IotDeviceActivity;
import com.nickem.model.IotDeviceStrategy;
import com.nickem.model.util.IotDeviceGenerator;
import com.nickem.streams.util.IotAccountStatisticSerde;
import com.nickem.streams.util.IotDeviceActivitySerde;
import com.nickem.streams.util.IotDeviceSerde;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import static com.nickem.streams.StreamApplication.IOT_ACCOUNT_STATISTIC_TOPIC;
import static com.nickem.streams.StreamApplication.IOT_DEVICE_ACTIVITY_TOPIC;
import static com.nickem.streams.StreamApplication.IOT_DEVICE_TOPIC;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StreamApplicationTest {

    private final StreamApplication application = new StreamApplication();
    private TopologyTestDriver testDriver;

    @BeforeEach
    public void setup() {}

    @AfterEach
    public void tearDown() {
        testDriver.close();
    }

    @Test
    public void testOne() {
        final String outputTopic = "output";
        final StreamsBuilder builder = new StreamsBuilder();
        builder.stream(IOT_DEVICE_TOPIC, Consumed.with(Serdes.String(), new IotDeviceSerde()))
               .groupBy((k, v) -> v.getAccountSerialNumber())
               .count()
               .toStream()
               .to(outputTopic, Produced.with(Serdes.String(), Serdes.Long()));

        final Topology topology = builder.build();

        final Properties config = application.getConfig();
        testDriver = new TopologyTestDriver(topology, config);

        final Collection<IotDevice> iotDevices = IotDeviceGenerator.generateIotDevice(10);

        final ConsumerRecordFactory<String, IotDevice> consumerFactory =
                new ConsumerRecordFactory<>(IOT_DEVICE_TOPIC, new StringSerializer(),
                                            new IotDeviceSerde().serializer());

        for (final IotDevice device : iotDevices) {
            testDriver.pipeInput(consumerFactory.create(IOT_DEVICE_TOPIC, device.getAccountSerialNumber(), device));
        }

        ProducerRecord<String, Long> outputRecord =
                testDriver.readOutput(outputTopic, new StringDeserializer(), new LongDeserializer());
        while (outputRecord != null) {

            System.out.println(String.format("Output record for [%s]: %s\n", outputRecord.key(), outputRecord.value()));
            outputRecord = testDriver.readOutput(outputTopic, new StringDeserializer(), new LongDeserializer());
        }
    }

    @Test
    public void testComplex() {
        final String destinationTopic = IOT_ACCOUNT_STATISTIC_TOPIC;
        final StreamsBuilder builder = new StreamsBuilder();
        final Topology topology =
                application.getComplexDeviceStreamTopologyImproved(builder, IOT_DEVICE_TOPIC, IOT_DEVICE_ACTIVITY_TOPIC,
                                                                   IOT_ACCOUNT_STATISTIC_TOPIC);

        final Properties config = application.getConfig();
        testDriver = new TopologyTestDriver(topology, config);

        final Collection<IotDevice> iotDevices = IotDeviceGenerator.generateIotDevice(3);
        final ConsumerRecordFactory<String, IotDevice> deviceFactory =
                new ConsumerRecordFactory<>(IOT_DEVICE_TOPIC, new StringSerializer(),
                                            new IotDeviceSerde().serializer());
        final ConsumerRecordFactory<String, IotDeviceActivity> deviceActivityFactory =
                new ConsumerRecordFactory<>(IOT_DEVICE_ACTIVITY_TOPIC, new StringSerializer(),
                                            new IotDeviceActivitySerde().serializer());

        for (final IotDevice device : iotDevices) {
            testDriver.pipeInput(deviceFactory.create(IOT_DEVICE_TOPIC, device.getDeviceSerialNumber(), device));

            final int activitiesCount = new Random().nextInt(10);
            System.out.printf("Activities count for %s: %s\n", device.getDeviceSerialNumber(), activitiesCount);
            final Pair<IotDevice, List<IotDeviceActivity>> iotDeviceToActivities =
                    IotDeviceGenerator.generateIotDeviceActivity(device, IotDeviceStrategy.STATIC, activitiesCount, 1L);

            for (final IotDeviceActivity activity : iotDeviceToActivities.getRight()) {
                testDriver.pipeInput(
                        deviceActivityFactory.create(IOT_DEVICE_ACTIVITY_TOPIC, UUID.randomUUID().toString(),
                                                     activity));
            }
        }

        ProducerRecord<String, IotAccountStatistic> outputRecord =
                testDriver.readOutput(destinationTopic, new StringDeserializer(),
                                      new IotAccountStatisticSerde().deserializer());
        while (outputRecord != null) {

            System.out.println(String.format("Output record for [%s]: %s", outputRecord.key(), outputRecord.value()));
            outputRecord = testDriver.readOutput(destinationTopic, new StringDeserializer(),
                                                 new IotAccountStatisticSerde().deserializer());
        }
    }

}
