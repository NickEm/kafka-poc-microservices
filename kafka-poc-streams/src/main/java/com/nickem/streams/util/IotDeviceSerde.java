package com.nickem.streams.util;

import com.nickem.model.IotDevice;

import org.apache.kafka.common.serialization.Serdes;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class IotDeviceSerde extends Serdes.WrapperSerde<IotDevice> {

    public IotDeviceSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(IotDevice.class));
    }
}
