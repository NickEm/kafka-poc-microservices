package com.nickem.streams.util;

import com.nickem.model.IotDeviceActivity;

import org.apache.kafka.common.serialization.Serdes;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class IotDeviceActivitySerde extends Serdes.WrapperSerde<IotDeviceActivity> {

    public IotDeviceActivitySerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(IotDeviceActivity.class));
    }
}
