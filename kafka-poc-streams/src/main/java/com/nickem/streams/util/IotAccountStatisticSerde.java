package com.nickem.streams.util;

import com.nickem.model.IotAccountStatistic;

import org.apache.kafka.common.serialization.Serdes;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class IotAccountStatisticSerde extends Serdes.WrapperSerde<IotAccountStatistic> {

    public IotAccountStatisticSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(IotAccountStatistic.class));
    }
}
