package com.nickem.streams;

import com.nickem.model.IotAccountStatistic;
import com.nickem.model.IotDevice;
import com.nickem.model.IotDeviceActivity;
import com.nickem.streams.util.IotAccountStatisticSerde;
import com.nickem.streams.util.IotDeviceActivitySerde;
import com.nickem.streams.util.IotDeviceSerde;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.DefaultProductionExceptionHandler;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Serialized;
import org.apache.kafka.streams.kstream.TimeWindowedDeserializer;
import org.apache.kafka.streams.kstream.TimeWindowedSerializer;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class StreamApplication {

    static final String IOT_DEVICE_TOPIC = "iot-device-1r-2p";
    static final String IOT_DEVICE_ACTIVITY_TOPIC = "iot-device-activity-1r-2p";
    static final String IOT_ACCOUNT_STATISTIC_TOPIC = "iot-account-statistic-1r-2p";
    private static final int WINDOW_DURATION_SECONDS = 60;

    public static void main(String[] args) throws InterruptedException {
        final StreamApplication application = new StreamApplication();
        final Properties props = getConfig();

        final StreamsBuilder builder = new StreamsBuilder();
        final Topology topology = application.getComplexDeviceStreamTopologyImproved(builder,
                                                                                     IOT_DEVICE_TOPIC,
                                                                                     IOT_DEVICE_ACTIVITY_TOPIC,
                                                                                     IOT_ACCOUNT_STATISTIC_TOPIC);

        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        streams.start();
        latch.await();
    }

    static Properties getConfig() {
        final Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-pipe");
        /*The value should be separated by commas if multiple brokers provided*/
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka:9092");
        //props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka1:19092,kafka2:19093,kafka3:19094");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, new JsonSerde<>().getClass());
        props.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG, LogAndContinueExceptionHandler.class);
        props.put(StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG, DefaultProductionExceptionHandler.class);
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, 1);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 30000);
        /*Replication factory should be <= to available brokers*/
        props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 1);
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        props.put(StreamsConfig.STATE_DIR_CONFIG, "/tmp");
        return props;
    }

    Topology getComplexDeviceStreamTopologyImproved(final StreamsBuilder builder, final String deviceTopic,
                                                    final String deviceActivityTopic,
                                                    final String accountStatisticTopic) {

        final GlobalKTable<String, IotDevice > iotDeviceTable =
                builder.globalTable(deviceTopic, Consumed.with(Serdes.String(), new IotDeviceSerde()));

        final Serializer<Windowed<String>> windowedSerializer = new TimeWindowedSerializer(new StringSerializer());
        final Deserializer<Windowed<String>> windowedDeserializer = new TimeWindowedDeserializer(new StringDeserializer());
        final Serde<Windowed<String>> windowedSerde = Serdes.serdeFrom(windowedSerializer, windowedDeserializer);

        builder.stream(deviceActivityTopic, Consumed.with(Serdes.String(), new IotDeviceActivitySerde()))
               .join(iotDeviceTable,
                     (key, deviceActivity) -> deviceActivity.getDeviceSerialNumber(),
                     (deviceActivity, device) -> IotDeviceActivity.builder()
                                                               .id(deviceActivity.getId())
                                                               .downloadBytes(deviceActivity.getDownloadBytes())
                                                               .uploadBytes(deviceActivity.getUploadBytes())
                                                               .sessionStartTime(deviceActivity.getSessionStartTime())
                                                               .accountSerialNumber(device.getAccountSerialNumber())
                                                               .build())
               .selectKey((k, v) -> v.getAccountSerialNumber())
               .groupByKey(Serialized.with(Serdes.String(), new IotDeviceActivitySerde()))
               .windowedBy(TimeWindows.of(Duration.ofSeconds(WINDOW_DURATION_SECONDS).toMillis()))
               .aggregate(IotAccountStatistic::new, (aggKey, newValue, aggValue) -> {
                   aggValue.setAccountSerialNumber(newValue.getAccountSerialNumber());
                   aggValue.setDownloadBytes(aggValue.getDownloadBytes() + newValue.getDownloadBytes());
                   aggValue.setUploadBytes(aggValue.getUploadBytes() + newValue.getUploadBytes());
                   return aggValue;
               }, Materialized.with(Serdes.String(), new IotAccountStatisticSerde()))
               .mapValues((k, v) -> {
                   v.setStartWindow(k.window().start());
                   v.setEndWindow(k.window().end());
                   return v;
               }, Materialized.with(windowedSerde, new IotAccountStatisticSerde()))
               .toStream()
               .map((k, v) -> new KeyValue<>(k.key(), v))
               .to(accountStatisticTopic, Produced.with(Serdes.String(), new IotAccountStatisticSerde()));

        return builder.build();
    }

}
