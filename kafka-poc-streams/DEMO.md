## Kafka Streams Demo App

### Description

This application execution flow:

* Producer side
	* Produces devices to topic
	 	* topic name is configured in [property file](../kafka-poc-producer/src/main/resources/application.yaml) by property`kafka-topics.iot-device.name`
	 	* data format class is [IotDevice](../kafka-poc-model/src/main/java/com/nickem/model/IotDevice.java)
	 	* iot-device-activity  
	* Produces device activities to topic
		* topic name is configured in [property file](../kafka-poc-producer/src/main/resources/application.yaml) by property`kafka-topics.iot-device-activity.name`
    	* data format class is [IotDeviceActivity](../kafka-poc-model/src/main/java/com/nickem/model/IotDeviceActivity.java)
	
* Streams side
	* reads data from specified topics and aggregate data based on input  
	* writes result data to topic `iot-account-statistic-1r-2p`
	* data format class for result is [IotAccountStatistic](../kafka-poc-model/src/main/java/com/nickem/model/IotAccountStatistic.java)

### Prerequisites
* You should have `docker` and `docker-compose` installed

### Installation

1. To build newest targets of all modules run:
 
		./gradlew clean build
2. To run application execute steps:

* Create new docker network that is used here for better localization:
 	
 		docker network create kafka-local-network

* Start kafka+zookeeper duet in the folder root:
	
		docker-compose -f docker-compose-kafka.yml up

* To create needed topics execute in terminal:
	
		docker exec confluent-kafka bash -c "\
        /usr/bin/kafka-topics --zookeeper zookeeper:2181 --create --topic iot-device-1r-2p --partitions 1 --replication-factor 1 && \
        /usr/bin/kafka-topics --zookeeper zookeeper:2181 --create --topic iot-device-activity-1r-2p --partitions 1 --replication-factor 1 && \
        /usr/bin/kafka-topics --zookeeper zookeeper:2181 --create --topic iot-account-statistic-1r-2p --partitions 1 --replication-factor 1"

* To listen to stream execution result execute in other terminal:
		
		docker exec -it confluent-kafka bash -c "/usr/bin/kafka-console-consumer --bootstrap-server kafka:9092 --topic iot-account-statistic-1r-2p --from-beginning"

* To start producer application execute in other terminal:		
		
		cd kafka-poc-producer && docker build -t kafka-producer . && cd ../
		docker run --rm --name kafka-producer -p 8081:8081 --network kafka-local-network -e SERVER_PORT=8081 kafka-producer

* To start streams application execute in other terminal:		
		
		cd kafka-poc-streams && docker build -t kafka-poc-streams . && cd ../
		docker run --rm --name kafka-poc-streams --network kafka-local-network kafka-poc-streams
		
* To generate devices execute in other terminal:
		
		curl -X POST 'http://localhost:8081/start/iot-devices'
		
* To generate device activities execute terminal:		
		
		curl -X POST 'http://localhost:8081/start/iot-activities'
		
* As a result you should be able to see which values where generated from `producer` side in it's console and compare 
them with result of listening to result topic 
