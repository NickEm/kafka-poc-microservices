To study

* [Skip intermediate result](https://stackoverflow.com/questions/38935904/how-to-send-final-kafka-streams-aggregation-result-of-a-time-windowed-ktable)
* [Joins explained](https://www.confluent.io/blog/crossing-streams-joins-apache-kafka/)
* [Stream examples](https://github.com/confluentinc/kafka-streams-examples)
* [Stream DSL](https://docs.confluent.io/5.3.0/streams/developer-guide/dsl-api.html)
* [ktable-vs-globalktable](https://stackoverflow.com/questions/45975755/what-are-the-differences-between-ktable-vs-globalktable-and-leftjoin-vs-outerj)
* [GlobalKTablesExample](https://github.com/confluentinc/kafka-streams-examples/blob/5.3.1-post/src/main/java/io/confluent/examples/streams/GlobalKTablesExample.java)