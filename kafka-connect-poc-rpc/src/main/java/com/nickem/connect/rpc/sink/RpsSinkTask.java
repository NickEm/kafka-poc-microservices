package com.nickem.connect.rpc.sink;

import com.nickem.connect.rpc.VersionUtil;

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.sink.SinkTask;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Slf4j
public class RpsSinkTask extends SinkTask {

    private OkHttpClient client;
    private RpsConnectorConfig config;

    @Override
    public String version() {
        return VersionUtil.getVersion();
    }

    @Override
    public void start(final Map<String, String> props) {
        log.info("Task initialization is started...");
        client = new OkHttpClient();
        config = new RpsConnectorConfig(props);
        log.info("Task initialization is finished.");
    }

    @Override
    public void put(final Collection<SinkRecord> collection) {
        for (final SinkRecord sinkRecord : collection) {

            final String recordStringValue = String.valueOf(sinkRecord.value());
            log.info("Processing record is: {}", recordStringValue);
            final RequestBody body = RequestBody.create(recordStringValue,
                                                        MediaType.parse("application/json; charset=utf-8"));

            final Request request = new Request.Builder()
                    .url(config.getResourceUrlBase() + config.getResourceUrlPath())
                    .post(body)
                    .build();

            final Call call = client.newCall(request);
            try (final Response response = call.execute()) {

                if (response.code() != 200 && response.code() != 201) {
                    log.warn("Service responded with code: {} to request with key: {}", response.code(), sinkRecord.key());
                }
            } catch (final IOException e) {
                log.error("An error appeared while processing record with key: {}. {}", sinkRecord.key(), e);
            }
        }
    }

    @Override
    public void flush(Map<TopicPartition, OffsetAndMetadata> map) {
        log.info("Flushing the queue.");
    }

    @Override
    public void stop() {
        try {
            client.dispatcher().executorService().shutdown();
            client.connectionPool().evictAll();
        } catch (final Exception e) {
            log.error("An error appeared while shutting down RPS client: {}", e);
        }
    }

}
