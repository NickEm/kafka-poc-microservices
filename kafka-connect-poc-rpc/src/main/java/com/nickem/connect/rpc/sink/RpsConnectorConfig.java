package com.nickem.connect.rpc.sink;

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigDef.Type;

import java.util.HashMap;
import java.util.Map;

public class RpsConnectorConfig extends AbstractConfig {

    private static final String RESOURCE_URL_BASE = "resource.url.base";
    private static final String RESOURCE_URL_PATH = "resource.url.path";
    private static final String RESOURCE_URL_BASE_DOC = "Resource URL base to which RPC should be performed.";
    private static final String RESOURCE_URL_PATH_DOC = "Resource URL path to which RPC should be performed.";

    public RpsConnectorConfig(ConfigDef config, Map<String, String> parsedConfig) {
        super(config, parsedConfig);
    }

    public RpsConnectorConfig(Map<String, String> parsedConfig) {
        this(conf(), parsedConfig);
    }

    public static ConfigDef conf() {
        return new ConfigDef()
                .define(RESOURCE_URL_BASE, Type.STRING, Importance.HIGH, RESOURCE_URL_BASE_DOC)
                .define(RESOURCE_URL_PATH, Type.STRING, Importance.HIGH, RESOURCE_URL_PATH_DOC);
    }

    public String getResourceUrlBase() {
        return this.getString(RESOURCE_URL_BASE);
    }

    public String getResourceUrlPath() {
        return this.getString(RESOURCE_URL_PATH);
    }

    public Map<String, String> toMap() {
        final Map<String, ?> uncastProperties = this.values();
        final Map<String, String> config = new HashMap<>(uncastProperties.size());
        uncastProperties.forEach((key, valueToBeCast) -> config.put(key, valueToBeCast.toString()));
        return config;
    }
}
