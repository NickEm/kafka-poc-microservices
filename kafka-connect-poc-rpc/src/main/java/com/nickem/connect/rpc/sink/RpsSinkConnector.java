package com.nickem.connect.rpc.sink;

import com.nickem.connect.rpc.VersionUtil;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.Task;
import org.apache.kafka.connect.sink.SinkConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RpsSinkConnector extends SinkConnector {

    private RpsConnectorConfig config;

    @Override
    public String version() {
        return VersionUtil.getVersion();
    }

    @Override
    public void start(final Map<String, String> map) {
        config = new RpsConnectorConfig(map);
    }

    @Override
    public Class<? extends Task> taskClass() {
        return RpsSinkTask.class;
    }

    @Override
    public List<Map<String, String>> taskConfigs(final int maxTasks) {
        log.info("Define max tasks count is: {}", maxTasks);
        final List<Map<String, String>> configs = new ArrayList<>(maxTasks);
        final Map<String, String> taskConfig = new HashMap<>(config.toMap());
        for (int i = 0; i < maxTasks; i++) {
            configs.add(taskConfig);
        }

        return configs;
    }

    @Override
    public void stop() {
        /*NOP*/
    }

    @Override
    public ConfigDef config() {
        return RpsConnectorConfig.conf();
    }
}
