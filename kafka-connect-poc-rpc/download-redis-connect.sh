#!/usr/bin/env bash

mkdir -p /tmp/custom/jars
curl "https://d1i4a15mxbxib1.cloudfront.net/api/plugins/jcustenborder/kafka-connect-redis/versions/0.0.2.9/jcustenborder-kafka-connect-redis-0.0.2.9.zip" -L -o /tmp/custom/jars/kafka-connect-redis-0.0.2.9.zip
unzip /tmp/custom/jars/kafka-connect-redis-0.0.2.9.zip -d /tmp/custom/jars/
rm /tmp/custom/jars/kafka-connect-redis-0.0.2.9.zip