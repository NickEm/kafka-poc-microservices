##### TODO: 

* Connect
	* [Main kafka docs](http://kafka.apache.org/documentation/#connect)
	* [distributed-mode](https://docs.confluent.io/current/connect/userguide.html#distributed-mode)
	* [install-connectors](https://docs.confluent.io/current/connect/managing/install.html#connect-install-connectors)
	* [connect-installing-plugins](https://docs.confluent.io/current/connect/userguide.html#connect-installing-plugins)
	* [installing-in-docker](https://stackoverflow.com/questions/56474734/how-to-run-kafka-connect-connectors-automatically-e-g-in-production)
	* [run-command-in-compose](https://stackoverflow.com/questions/30063907/using-docker-compose-how-to-execute-multiple-commands)
	* [kafka-connect-jdbc](https://docs.confluent.io/5.3.1/connect/kafka-connect-jdbc/)
	* [connect-deep-dive-jdbc](https://www.confluent.io/blog/kafka-connect-deep-dive-jdbc-source-connector)
	* [connect-pipeline](https://www.confluent.de/blog/simplest-useful-kafka-connect-data-pipeline-world-thereabouts-part-1/)
	* [Rest API](https://docs.confluent.io/5.3.1/connect/references/restapi.html#connect-userguide-rest)
	* [How many connector task should we use](https://stackoverflow.com/questions/41900694/ideal-value-for-kafka-connect-distributed-tasks-max-configuration-setting/42030617#42030617)
	* [Write you connector explained](https://opencredo.com/blogs/kafka-connect-source-connectors-a-detailed-guide-to-connecting-to-what-you-love/)
	
* Transformations
	* https://docs.confluent.io/5.3.1/connect/references/restapi.html#connect-userguide-rest
* Converters
	* https://rmoff.net/2019/05/08/when-a-kafka-connect-converter-is-not-a-_converter_/



	 