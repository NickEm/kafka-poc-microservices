/*! SET storage_engine=INNODB */;

drop table if exists play_store_app;
create table play_store_app (
  record_id serial
, app varchar(256) null
, category varchar(64) null
, rating double precision null
, reviews int null
, size varchar(64) null
, installs varchar(16) null
, type varchar(128) null
, price double precision null
, content_rating varchar(32) null
, genres varchar(128) null
, last_updated date null
, current_ver varchar(32) null
, android_ver varchar(32) null
, primary key(record_id)
) /*! CHARACTER SET utf8 COLLATE utf8_bin */;