package com.nickem.kafkaconsumer.dao;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PlayStoreAppKeys {

    public static final String RECORD_ID_COLUMN_KEY = "record_id";

    public static final String APP_COLUMN_KEY = "app";
    public static final String CATEGORY_COLUMN_KEY = "category";
    public static final String RATING_COLUMN_KEY = "rating";
    public static final String REVIEWS_COLUMN_KEY = "reviews";
    public static final String SIZE_COLUMN_KEY = "size";
    public static final String INSTALLS_COLUMN_KEY = "installs";
    public static final String TYPE_COLUMN_KEY = "type";
    public static final String PRICE_COLUMN_KEY = "price";
    public static final String CONTENT_RATING_COLUMN_KEY = "content_rating";
    public static final String GENRES_COLUMN_KEY = "genres";
    public static final String LAST_UPDATED_COLUMN_KEY = "last_updated";
    public static final String CURRENT_VER_COLUMN_KEY = "current_ver";
    public static final String ANDROID_VER_COLUMN_KEY = "android_ver";
    
}
