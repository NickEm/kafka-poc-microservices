package com.nickem.kafkaconsumer.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.ANDROID_VER_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.APP_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.CATEGORY_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.CONTENT_RATING_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.CURRENT_VER_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.GENRES_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.INSTALLS_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.LAST_UPDATED_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.PRICE_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.RATING_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.RECORD_ID_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.REVIEWS_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.SIZE_COLUMN_KEY;
import static com.nickem.kafkaconsumer.dao.PlayStoreAppKeys.TYPE_COLUMN_KEY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "play_store_app")
public class PlayStoreAppEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = RECORD_ID_COLUMN_KEY)
    private long id;

    @Column(name = APP_COLUMN_KEY)
    private String app;

    @Column(name = CATEGORY_COLUMN_KEY)
    private String category;

    @Column(name = RATING_COLUMN_KEY)
    private Double rating;

    @Column(name = REVIEWS_COLUMN_KEY)
    private Integer reviews;

    @Column(name = SIZE_COLUMN_KEY)
    private String size;

    @Column(name = INSTALLS_COLUMN_KEY)
    private String installs;

    @Column(name = TYPE_COLUMN_KEY)
    private String type;

    @Column(name = PRICE_COLUMN_KEY)
    private double price;

    @Column(name = CONTENT_RATING_COLUMN_KEY)
    private String contentRating;

    @Column(name = GENRES_COLUMN_KEY)
    private String genres;

    @Column(name = LAST_UPDATED_COLUMN_KEY)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate lastUpdated;

    @Column(name = CURRENT_VER_COLUMN_KEY)
    private String currentVer;

    @Column(name = ANDROID_VER_COLUMN_KEY)
    private String androidVer;

}
