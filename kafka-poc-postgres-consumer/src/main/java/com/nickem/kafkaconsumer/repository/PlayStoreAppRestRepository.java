package com.nickem.kafkaconsumer.repository;

import com.nickem.kafkaconsumer.entity.PlayStoreAppEntity;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "play-store-apps", path = "play-store-apps")
public interface PlayStoreAppRestRepository extends PagingAndSortingRepository<PlayStoreAppEntity, Long> {

    List<PlayStoreAppEntity> findByApp(@Param("app") String app);

}

