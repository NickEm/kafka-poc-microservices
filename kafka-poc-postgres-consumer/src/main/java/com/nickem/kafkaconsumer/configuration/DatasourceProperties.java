package com.nickem.kafkaconsumer.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "kafka-portgres-consumer.datasource")
public class DatasourceProperties {

    private String driverClassName;
    private String jdbcUrl;
    private String name;
    private String username;
    private String password;

    private String poolName;
    private int proxyPort;
    private int maximumPoolSize;
    private int minimumIdle;
    private int connectionTimeout;

    private String dialect;

}
