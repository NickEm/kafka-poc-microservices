Kafka Postgres Consumer

Installation

1. To build new docker image execute:
 
		./gradlew clean build && docker build -t kafka-postgres-consumer .
        ./gradlew :kafka-poc-postgres-consumer:clean :kafka-poc-postgres-consumer:build && cd kafka-poc-postgres-consumer && docker build -f Dockerfile -t kafka-poc-postgres-consumer . && cd ../
2. To run it execute:

		docker run --rm --name kafka-poc-postgres-consumer -p 8091:8091 --network kafka-local-network -e SERVER_PORT=8091 kafka-poc-postgres-consumer
		
		docker run --rm --name kafka-poc-postgres-consumer -p 8091:8091 --network kafka-local-network \
        -e SERVER_PORT=8091 \
        -e SPRING_DATASOURCE_NAME=kafka_consumer \
        -e SPRING_DATASOURCE_URL=jdbc:postgresql://kafka-postgres-db:5432/kafka_consumer \
        -e SPRING_DATASOURCE_USERNAME=root \
        -e SPRING_DATASOURCE_PASSWORD=kafka_ems_db_29 \
        kafka-poc-postgres-consumer


Installation on Java 11:

1. Docker image:

		./gradlew :kafka-poc-postgres-consumer:clean :kafka-poc-postgres-consumer:build && cd kafka-poc-postgres-consumer && docker build -f Dockerfile-java-11 -t kafka-poc-postgres-consumer .
