##### Q&A

- How to persist data in topic for infinite amount of time
	- If you set broker-wide config to `log.retention.ms=-1`, all topics by default will have an infinite retention period.
	Likewise, if you set `retention.ms=-1` on a specific topic, it will have an infinite retention period.
	
- log-retention-and-cleanup
	- https://medium.com/@sunny_81705/kafka-log-retention-and-cleanup-policies-c8d9cb7e09f8
	
##### TODO
		